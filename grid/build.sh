#!/usr/bin/env bash


if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
    set -eu
fi

function build_in_dir() {
    # build main ntuple dumper
    BASE=$1
    BUILD_DIR=$2
    echo "building $BASE in $BUILD_DIR"

    if [[ -d ${BUILD_DIR} ]]; then
        rm -r ${BUILD_DIR}
    fi
    mkdir ${BUILD_DIR}
    pushd .
    cd ${BUILD_DIR}
    cmake ../${BASE}/
    make
    popd
}

# in case we have special packages checked out
if [[ -d athena ]]; then
    build_in_dir athena/Projects/WorkDir build-athena
fi

build_in_dir training-dataset-dumper build
