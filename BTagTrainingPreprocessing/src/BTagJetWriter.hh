#ifndef BTAG_JET_WRITER_HH
#define BTAG_JET_WRITER_HH


namespace H5 {
  class Group;
}
namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
}

class BTagJetWriterConfig;
struct SubstructureAccessors;

struct BTagJetWriterConfig;
struct BTagVariableMaps;

class JetOutputWriter;
class JetConsumers;

#include <string>
#include <vector>
#include <map>

// structure to serve as base for lambda functions
struct JetOutputs {
  JetOutputs();
  const xAOD::Jet* jet;
  const xAOD::Jet* parent;
  const xAOD::EventInfo* event_info;
};

class BTagJetWriter
{
public:
  BTagJetWriter(
    H5::Group& output_file,
    const BTagJetWriterConfig& jet);
  ~BTagJetWriter();
  BTagJetWriter(BTagJetWriter&&);
  BTagJetWriter(BTagJetWriter&) = delete;
  BTagJetWriter operator=(BTagJetWriter&) = delete;
  void write(const xAOD::Jet& jet, const xAOD::EventInfo* = 0);
  void write_dummy();
  void write_with_parent(const xAOD::Jet& jet,
                         const xAOD::Jet& parent,
                         const xAOD::EventInfo* = 0);
private:
  template<typename I, typename O = I>
  void add_btag_fillers(JetConsumers&,
                        const std::vector<std::string>&,
                        const BTagVariableMaps&,
                        O default_value = O() );
  template<typename I, typename O = I>
  void add_jet_fillers(JetConsumers&,
                       const std::vector<std::string>&,
                       O default_value = O());
  void add_jet_int_variables(JetConsumers&,
                        const std::vector<std::string>&);
  void add_parent_jet_int_variables(JetConsumers&,
                               const std::vector<std::string>&);
  void add_parent_fillers(JetConsumers&);
  void add_substructure(JetConsumers&);
  void add_event_info(JetConsumers&);
  bool m_write_parent;
  JetOutputWriter* m_hdf5_jet_writer;
  SubstructureAccessors* m_ssa;
};


#endif
