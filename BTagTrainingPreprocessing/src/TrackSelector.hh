#ifndef TRACK_SELECTOR_HH
#define TRACK_SELECTOR_HH

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODJet/Jet.h"
#include "TrackSelectorConfig.hh"

class TrackSelectorConfig;

class TrackSelector
{
public:
  TrackSelector(TrackSelectorConfig = TrackSelectorConfig());
  typedef std::vector<const xAOD::TrackParticle*> Tracks;
  Tracks get_tracks(const xAOD::Jet& jet) const;

private:
  typedef SG::AuxElement AE;
  typedef std::vector<ElementLink<xAOD::TrackParticleContainer> > TrackLinks;
  AE::ConstAccessor<TrackLinks> m_track_associator;

  bool passed_cuts(const xAOD::TrackParticle &tp) const;
  TrackSelectorConfig m_track_select_cfg;
};

#endif
